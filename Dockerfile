FROM nginx:1.21.3-alpine

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY hello.txt /var/www/

CMD ["nginx", "-g", "daemon off;"]