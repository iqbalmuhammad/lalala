variable "aws_region" {
  description = "AWS region"
  type        = string
  default     = "us-west-2"
}

variable "vpc_cidr_block" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "instance_type" {
  description = "Instance type"
  type        = string
  default     = "t2.medium"
}

variable "aws_amis" {
  default = {
    "us-west-2" = "ami-0ceee60bcb94f60cd"
  }
}

variable "key_name" {
  description = "Name of AWS key pair"
}