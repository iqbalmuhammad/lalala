terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

# terraform {
#   backend "s3" {
#     bucket = "lalala"
#     key    = "lalala-tfstate"
#     region = "us-west-2"
#   }
# }

resource "aws_vpc" "vpc" {
  cidr_block = var.vpc_cidr_block
 
  tags = { 
    Environment = "production" 
  } 
} 


resource "aws_subnet" "private" { 
  vpc_id     = aws_vpc.vpc.id 
  cidr_block = "10.0.1.0/24" 
 
  tags = { 
    Environment = "production" 
  } 
} 
 
resource "aws_subnet" "public" {  
  vpc_id     = aws_vpc.vpc.id 
  cidr_block = "10.0.2.0/24" 
 
  tags = { 
    Environment = "production" 
  } 
} 


resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id 
 
  tags = { 
    Environment = "production" 
  } 
} 

resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc.id 
  route { 
    cidr_block = "0.0.0.0/0" 
    gateway_id = aws_internet_gateway.igw.id 
  } 
  tags = { 
    Environment = "production" 
  } 
} 
 
resource "aws_route_table_association" "public" { 
  subnet_id      = aws_subnet.public.id 
  route_table_id = aws_route_table.public.id 
} 


resource "aws_eip" "nat" { 
  vpc      = true 
} 
 
resource "aws_nat_gateway" "ngw" { 
  allocation_id = aws_eip.nat.id 
  subnet_id = aws_subnet.public.id 
} 
 
resource "aws_route_table" "private" { 
  vpc_id = aws_vpc.vpc.id 
  route { 
    cidr_block = "0.0.0.0/0" 
    nat_gateway_id = aws_nat_gateway.ngw.id 
  } 
 
  tags { 
    Environment = "production" 
  } 
} 
 
resource "aws_route_table_association" "private" {
  subnet_id = aws_subnet.private.id 
  route_table_id = aws_route_table.private.id 
} 

resource "aws_placement_group" "test" {
  name     = "test"
  strategy = "cluster"
}

resource "aws_autoscaling_policy" "asg" {
  name                   = "foobar3-terraform-test"
  scaling_adjustment     = 4
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 300
  autoscaling_group_name = "${aws_autoscaling_group.asg.name}"

  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 45.0
  }
}

resource "aws_launch_configuration" "service" {
  name          = "service-lc"
  image_id      = var.aws_amis[var.aws_region]
  instance_type = var.instance_type

  user_data       = file("docker.sh")
  key_name        = var.key_name
}

resource "aws_autoscaling_group" "asg" {
  name                      = "private-asg"
  max_size                  = 5
  min_size                  = 2
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 4
  force_delete              = true
  launch_configuration      = aws_launch_configuration.service.name
  vpc_zone_identifier       = [aws_subnet.private.id]

  launch_template {
    id      = aws_launch_template.lalala.id
    version = "$Latest"
  }

  initial_lifecycle_hook {
    name                 = "foobar"
    default_result       = "CONTINUE"
    heartbeat_timeout    = 2000
    lifecycle_transition = "autoscaling:EC2_INSTANCE_LAUNCHING"

    notification_metadata = <<EOF
{
  Environment = "production"
}
EOF

    notification_target_arn = "arn:aws:sqs:us-east-1:444455556666:queue1*"
    role_arn                = "arn:aws:iam::123456789012:role/S3Access"
  }

  tag {
    Environment = "production"
    propagate_at_launch = true
  }

  timeouts {
    delete = "15m"
  }

  tag {
    Environment = "production"
    propagate_at_launch = false
  }
}